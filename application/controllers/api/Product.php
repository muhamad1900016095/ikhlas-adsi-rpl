<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;



class Product extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model','product');
    }


    public function index_get()
    {
        $id_product = $this->get('id_product');
        if ($id_product === null) {
            $product = $this->product->getProduct();
        } else {
            $product = $this->product->getProduct($id_product);
        }
        
        if ($product){
            $this->response( [
                'status' => true,
                'data' => $product
            ], RestController::HTTP_OK );
        } else{
            $this->response( [
                'status' => false,
                'message' => 'id tidak ditemukan'
            ], RestController::HTTP_NOT_FOUND );
        }
    }

    public function index_delete()
    {
        $id_product = $this->delete('id_product');
        if ($id_product === null){
            $this->response( [
                'status' => false,
                'message' => 'provide an id'
            ], RestController::HTTP_BAD_REQUEST );
        }else{
            if( $this->product->deleteProduct($id_product) > 0) {
                //ok
                $this->response([
                    'status' => true,
                    'id_product' => $id_product,
                    'message' => 'deleted'
                ], RestController::HTTP_OK);
            }else{
                //id not found
                $this->response([
                    'status' => false,
                    'message' => 'id ga nemu '
                ], RestController::HTTP_BAD_REQUEST);
            }
            
        }
    }

    public function index_post()
    {
        $data= [
            'product_name' => $this->post('product_name'),
            'price' => $this->post('price'),
            'description' => $this->post('description')
            
        ];

        if($this->product->createProduct($data) > 0){
            $this->response([
                'status' => true,
                'message' => 'Product has been added'
            ], RestController::HTTP_CREATED);
        }else{
            //id not found
            $this->response([
                'status' => false,
                'message' => 'failed to add product '
            ], RestController::HTTP_BAD_REQUEST);
        }
    }

    public function index_put()
    {
        $id_product = $this->put('id_product');
        $data= [
            'product_name' => $this->put('product_name'),
            'price' => $this->put('price'),
            'description' => $this->put('description')
        ];

        if($this->product->updateProduct($data, $id_product) > 0){
            $this->response([
                'status' => true,
                'message' => 'Product has been updated'
            ], RestController::HTTP_OK);
        }else{
            //id not found
            $this->response([
                'status' => false,
                'message' => 'failed to update product '
            ], RestController::HTTP_BAD_REQUEST);
        }

    }
}