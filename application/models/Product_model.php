<?php

class Product_model extends CI_Model
{
    public function getProduct($id_product = null)
    {
        if ($id_product === null){
            return $this->db->get('katalog_produk')->result_array();
        }else{
            return $this->db->get_where('katalog_produk', ['id_product' => $id_product])->result_array();
        }
    }

    public function deleteProduct($id_product)
    {
        $this->db->delete('katalog_produk', ['id_product' => $id_product]);
        return $this->db->affected_rows();
    }
    public function createProduct($data)
    {
        $this->db->insert('katalog_produk', $data);
        return $this->db->affected_rows();
    }
    public function updateProduct($data, $id_product)
    {
        $this->db->update('katalog_produk', $data, ['id_product'=>$id_product]);
        return $this->db->affected_rows();
    }
}